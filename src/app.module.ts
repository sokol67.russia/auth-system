import { Module } from '@nestjs/common'
import {AuthModule} from "@/modules/auth/auth.module"
import {EnvironmentModule} from "@/modules/environment/environment.module"
import {RolesModule} from "@/modules/roles/roles.module";
import {UsersModule} from "@/modules/users/users.module";
import {AppService} from "@/app.service";
import {AppController} from "@/app.controller";
import {DatabaseModule} from "@/modules/database/database.module";

@Module({
  imports: [
    DatabaseModule,
    AuthModule,
    EnvironmentModule,
    UsersModule,
    RolesModule
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
