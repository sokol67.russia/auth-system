import { NestFactory } from '@nestjs/core'
import {AppModule} from "@/app.module";
import { config } from 'dotenv'
import cors from 'cors'

async function bootstrap() {
  config()
  const app = await NestFactory.create(AppModule)
  app.enableCors()
  app.setGlobalPrefix('api')
  app.use(cors())
  await app.listen(process.env.PORT || 3000)
}

bootstrap()
