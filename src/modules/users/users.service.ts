import {Inject, Injectable, OnModuleInit} from '@nestjs/common'
import {SequelizeService} from '@/lib/sequelize.service'
import {FindOptions} from 'sequelize'
import User from "@/modules/users/user.entity";
import {usersRepositoryProvider} from "@/modules/users/users.providers";

@Injectable()
export class UsersService extends SequelizeService<User> {
  constructor(
    @Inject(usersRepositoryProvider.provide)
    readonly repository: typeof User,
  ) {
    super(repository)
  }

  async findByPhone(phone: string, options?: Exclude<FindOptions, 'where'>) {
    return this.repository.findOne({ where: { phone }, ...options })
  }
}
