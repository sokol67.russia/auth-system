import User from "@/modules/users/user.entity";
import { Provider, ValueProvider } from '@nestjs/common/interfaces'

export const usersRepositoryProvider: ValueProvider = {
  provide: 'UsersRepository',
  useValue: User
}

export const usersProviders: Provider[] = [usersRepositoryProvider]
