import * as path from 'path'
import { Global, Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import {EnvironmentService} from "@/modules/environment/environment.service";

@Global()
@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: path.resolve(__dirname, '../../../.env')
    })
  ],
  providers: [EnvironmentService],
  exports: [EnvironmentService]
})
export class EnvironmentModule {}
