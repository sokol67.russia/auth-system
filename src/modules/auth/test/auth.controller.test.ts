import {Test} from '@nestjs/testing'
import {AuthController} from "@/modules/auth/auth.controller"
import {AuthService} from "@/modules/auth/auth.service"
import {ModuleMocker} from 'jest-mock'
import User from "@/modules/users/user.entity";
import {Request} from "express";

const moduleMocker = new ModuleMocker(global)

describe('AuthController', () => {
  let authController: AuthController
  let authService: AuthService

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [AuthController],
      // providers: [AuthService, UsersService, JwtService, EnvironmentService, usersRepositoryProvider],
    }).useMocker((token) => {
        if (token === AuthService) {
          return {
            login: jest.fn().mockResolvedValue(Promise.resolve(['token', {}])),
            createAuthToken: jest.fn().mockResolvedValue('token')
          }
        }
        if (typeof token === 'function') {
          const mockMetadata = moduleMocker.getMetadata(token)
          const Mock = moduleMocker.generateFromMetadata(mockMetadata)
          return new Mock()
        }
      }).compile()

    authService = moduleRef.get<AuthService>(AuthService)
    authController = moduleRef.get<AuthController>(AuthController)
  })

  describe('login', () => {
    it('should call the login service method', async () => {
      // const result: [string, User] = ['token', new User()]
      // jest.spyOn(authService, 'login').mockImplementation(() => Promise.resolve(result));

      const result = await authController.login('+79605897860', 'testLogin', 'password')
      expect(authService.login).toHaveBeenCalled()
      expect(authService.login).toHaveBeenCalledWith('+79605897860', 'password')
      expect(result).toEqual(expect.objectContaining({ token: 'token' }))
    })
  })

  describe('updateToken', () => {
    it('should call the createAuthToken service method', async () => {
      const user: User = { id: 1, name: 'test', roleId: 1 } as User
      // TODO:: what is unknown??
      const result = await authController.updateToken({ user } as unknown as Request, 'expiresIn')
      expect(authService.createAuthToken).toHaveBeenCalled()
      expect(authService.createAuthToken).toHaveBeenCalledWith(user, 'expiresIn')
      // TODO:: Strange Promise
      expect(result).toEqual(expect.objectContaining({ token: 'token' }))
    })
  })
})
