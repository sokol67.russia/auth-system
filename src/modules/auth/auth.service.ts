import sha1 from 'sha1'
import { BadRequestException, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import { TokenExpiredError } from 'jsonwebtoken'
import {EnvironmentService} from "@/modules/environment/environment.service";
import {UsersService} from "@/modules/users/users.service";
import User from "@/modules/users/user.entity";
import {DecodedToken} from '@/modules/auth/interfaces/decoded-token.interface'
import {TokenPayload} from '@/modules/auth/interfaces/token-payload.interface'

@Injectable()
export class AuthService {
  static secret: string

  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    private readonly envService: EnvironmentService
  ) {
    AuthService.secret = envService.get('JWT_SECRET', 'superSecret')
  }

  async login(phone: string, password: string): Promise<[string, User]> {
    if (!phone) throw new BadRequestException('Не указан телефон')
    const user = await this.usersService.findByPhone(phone)

    if (!user) {
      throw new NotFoundException(`Пользователь с таким телефоном не найден`)
    } else if (user.password !== AuthService.createPasswordHash(password)) {
      throw new UnauthorizedException('Неверный пароль')
    }

    const token = this.createAuthToken(user)
    return [token, user]
  }

  createAuthToken(user: User, expiresIn = '30d'): string {
    if (!user) throw new NotFoundException(`Пользователь не найден`)
    const payload: TokenPayload = { userId: user.id }
    return this.jwtService.sign(payload, { expiresIn })
  }

  validateAuthToken(token: string): DecodedToken {
    if (!token) throw new UnauthorizedException('Отсутствует токен аутентификации')

    try {
      return this.jwtService.verify(token) as DecodedToken
    } catch (e) {
      const text = e.expiredAt as TokenExpiredError ? `Токен аутентификации просрочен` : 'Токен аутентификации невалидный'
      throw new UnauthorizedException(text)
    }
  }

  static createPasswordHash(password = ''): string {
    const middleIndex = Math.round(password.length / 2)
    const start = password.slice(0, middleIndex)
    const end = password.slice(middleIndex, password.length)
    return sha1(`${start}${this.secret}${end}`)
  }

  static validatePassword(password: string): boolean {
    if (!password) throw new BadRequestException('Не указан пароль')
    if (password.length < 5) throw new BadRequestException('Пароль не может быть короче 5 символов')
    return true
  }
}
