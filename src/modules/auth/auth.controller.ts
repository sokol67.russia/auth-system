import { All, Body, Controller, HttpCode, HttpStatus, Post, Query, Req, UseGuards } from '@nestjs/common'
import { Request } from 'express'
import {AuthService} from "@/modules/auth/auth.service";
import {AuthGuard} from "@/modules/auth/auth.guard";
import User from "@/modules/users/user.entity";

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @HttpCode(HttpStatus.OK)
  @Post(['login', 'sign-in'])
  async login(@Body('phone') phone: string, @Body('login') login: string, @Body('password') password: string) {
    const [token] = await this.authService.login(phone || login, password)
    return { token }
  }

  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  @All(['/update-token', 'exchange'])
  async updateToken(@Req() req: Request, @Query('expiresIn') expiresIn?: string) {
    return {
      token: await this.authService.createAuthToken(req.user as User, expiresIn)
    }
  }
}
