import {Inject, Injectable, OnModuleInit} from '@nestjs/common'
import {SequelizeService} from '@/lib/sequelize.service'
import Role from "@/modules/roles/role.entity";
import {rolesRepositoryProvider} from "@/modules/roles/roles.providers";

@Injectable()
export class RolesService extends SequelizeService<Role> {
  constructor(
    @Inject(rolesRepositoryProvider.provide)
    readonly repository: typeof Role,
  ) {
    super(repository)
  }
}
