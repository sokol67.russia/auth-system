import { Provider, ValueProvider } from '@nestjs/common/interfaces'
import Role from "@/modules/roles/role.entity";

export const rolesRepositoryProvider: ValueProvider = {
  provide: 'RolesRepository',
  useValue: Role
}

export const rolesProviders: Provider[] = [rolesRepositoryProvider]
