import { Global, Module } from '@nestjs/common'
import {databaseProviders} from "@/modules/database/database.providers";
import {EnvironmentModule} from "@/modules/environment/environment.module";

@Global()
@Module({
  imports: [EnvironmentModule],
  providers: [...databaseProviders],
  exports: [...databaseProviders]
})
export class DatabaseModule {}
